# EndPoints
Utilizando el paquete de entity framework se realizo la migracion de las clases a la base de datos 

adjunto backup de la base de datos.

¿por que hacerlo con entity framework?
por que si ya hay algo que nos facilita la vida por que no usarlo.


# EndPoints

#### GET
http://localhost:59904/api/producto/

RESULT OK

>[
    {
        "id_producto": 1,
        "codigo": 1,
        "Descripcion_producto": "preuab",
        "precio": 2.0,
        "stock": 1,
        "iva": 12,
        "peso": 12.0,
        "Marca": null,
        "id_marca": null,
        "Presentacion": null,
        "id_presentacion": null,
        "Proveedor": null,
        "id_provvedor": null,
        "Zona": null,
        "id_zona": null
    },
    {
        "id_producto": 2,
        "codigo": 1,
        "Descripcion_producto": "preuasdsadab",
        "precio": 2.0,
        "stock": 1,
        "iva": 12,
        "peso": 12.0,
        "Marca": null,
        "id_marca": 3,
        "Presentacion": null,
        "id_presentacion": null,
        "Proveedor": null,
        "id_provvedor": null,
        "Zona": null,
        "id_zona": null
    },
    {
        "id_producto": 3,
        "codigo": 1,
        "Descripcion_producto": "preuasdsadab",
        "precio": 2.0,
        "stock": 1,
        "iva": 12,
        "peso": 12.0,
        "Marca": null,
        "id_marca": 4,
        "Presentacion": null,
        "id_presentacion": null,
        "Proveedor": null,
        "id_provvedor": null,
        "Zona": null,
        "id_zona": null
    },
    {
        "id_producto": 4,
        "codigo": 1,
        "Descripcion_producto": "preuasdsadab",
        "precio": 2.0,
        "stock": 1,
        "iva": 12,
        "peso": 12.0,
        "Marca": null,
        "id_marca": 1,
        "Presentacion": null,
        "id_presentacion": null,
        "Proveedor": null,
        "id_provvedor": null,
        "Zona": null,
        "id_zona": null
    },
    {
        "id_producto": 5,
        "codigo": 1,
        "Descripcion_producto": "prueba 2",
        "precio": 2.0,
        "stock": 1,
        "iva": 12,
        "peso": 12.0,
        "Marca": null,
        "id_marca": 1,
        "Presentacion": null,
        "id_presentacion": 1,
        "Proveedor": null,
        "id_provvedor": 1,
        "Zona": null,
        "id_zona": 1
    }
]

#### POST
http://localhost:59904/api/producto/

json insertar:
>{
    "id_producto": 1,
    "id_marca": 1,
    "id_presentacion": 1,
    "id_provvedor": 1,
    "id_zona": 1,
    "codigo": 1,
    "Descripcion_producto": "prueba 2",
    "precio": 2.0,
    "stock": 1,
    "iva": 12,
    "peso": 12.0
}

#### DELETE
http://localhost:59904/api/producto?id=1


result OK
{
    "id_producto": 1,
    "codigo": 1,
    "Descripcion_producto": "preuab",
    "precio": 2.0,
    "stock": 1,
    "iva": 12,
    "peso": 12.0,
    "Marca": null,
    "id_marca": null,
    "Presentacion": null,
    "id_presentacion": null,
    "Proveedor": null,
    "id_provvedor": null,
    "Zona": null,
    "id_zona": null
}

#### PUT

http://localhost:59904/api/producto?id=6

>{
    "id_producto": 6,
    "codigo": 1,
    "Descripcion_producto": "preuasdsadab",
    "precio": 2.0,
    "stock": 1,
    "iva": 12,
    "peso": 12.0,
    "id_marca": 1,
    "id_presentacion": 2,
    "id_provvedor": 2,
    "id_zona": 1
}
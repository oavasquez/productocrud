﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProductoCRUD.EntityModel
{
    [Table("Presentacion")]
    public class Presentacion
    {
        [Key]
        public int id_presentacion { get; set; }
        [StringLength(50)]
        public string descripcion { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProductoCRUD.EntityModel
{
    [Table("Proveedor")]
    public class Proveedor
    {
        [Key]
        public int id_proveedor { get; set; }
        [StringLength(100)]
        public string descripcion { get; set; }
    }
}
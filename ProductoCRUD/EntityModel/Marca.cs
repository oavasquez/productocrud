﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProductoCRUD.EntityModel
{
    [Table("Marca")]
    public class Marca
    {
        [Key]
        public int id_marca { get; set; }
        [StringLength(100)]
        public string descripcion { get; set; }
    }
}
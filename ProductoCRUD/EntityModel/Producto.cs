﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProductoCRUD.EntityModel
{
    [Table("Producto")]
    public class Producto
    {
        [Key]
        public int id_producto { get; set; }
      
      
        public int codigo { get; set; }
        [StringLength(1000)]
        public string Descripcion_producto { get; set; }
        public System.Nullable<Double> precio { get; set; }
        public int stock { get; set; }
        public int iva { get; set; }
        public System.Nullable<Double> peso { get; set; }

        
        public Marca Marca { get; set; }
        public System.Nullable<int> id_marca { get; set; }

        public Presentacion Presentacion { get; set; }
        public System.Nullable<int> id_presentacion { get; set; }

        public Proveedor Proveedor { get; set; }
        public System.Nullable<int> id_provvedor { get; set; }

        public Zona Zona { get; set; }
        public System.Nullable<int> id_zona { get; set; }
      

    }
}
﻿namespace ProductoCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        id_producto = c.Int(nullable: false, identity: true),
                        codigo = c.Int(nullable: false),
                        Descripcion_producto = c.String(maxLength: 1000),
                        precio = c.Double(),
                        stock = c.Int(nullable: false),
                        iva = c.Int(nullable: false),
                        peso = c.Double(),
                        id_marca_id_marca = c.Int(),
                        id_presentacion_id_presentacion = c.Int(),
                        id_provvedor_id_proveedor = c.Int(),
                        id_zona_id_zona = c.Int(),
                    })
                .PrimaryKey(t => t.id_producto)
                .ForeignKey("dbo.Marca", t => t.id_marca_id_marca)
                .ForeignKey("dbo.Presentacion", t => t.id_presentacion_id_presentacion)
                .ForeignKey("dbo.Proveedor", t => t.id_provvedor_id_proveedor)
                .ForeignKey("dbo.Zona", t => t.id_zona_id_zona)
                .Index(t => t.id_marca_id_marca)
                .Index(t => t.id_presentacion_id_presentacion)
                .Index(t => t.id_provvedor_id_proveedor)
                .Index(t => t.id_zona_id_zona);
            
            CreateTable(
                "dbo.Marca",
                c => new
                    {
                        id_marca = c.Int(nullable: false, identity: true),
                        descripcion = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.id_marca);
            
            CreateTable(
                "dbo.Presentacion",
                c => new
                    {
                        id_presentacion = c.Int(nullable: false, identity: true),
                        descripcion = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id_presentacion);
            
            CreateTable(
                "dbo.Proveedor",
                c => new
                    {
                        id_proveedor = c.Int(nullable: false, identity: true),
                        descripcion = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.id_proveedor);
            
            CreateTable(
                "dbo.Zona",
                c => new
                    {
                        id_zona = c.Int(nullable: false, identity: true),
                        descripcion = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id_zona);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Producto", "id_zona_id_zona", "dbo.Zona");
            DropForeignKey("dbo.Producto", "id_provvedor_id_proveedor", "dbo.Proveedor");
            DropForeignKey("dbo.Producto", "id_presentacion_id_presentacion", "dbo.Presentacion");
            DropForeignKey("dbo.Producto", "id_marca_id_marca", "dbo.Marca");
            DropIndex("dbo.Producto", new[] { "id_zona_id_zona" });
            DropIndex("dbo.Producto", new[] { "id_provvedor_id_proveedor" });
            DropIndex("dbo.Producto", new[] { "id_presentacion_id_presentacion" });
            DropIndex("dbo.Producto", new[] { "id_marca_id_marca" });
            DropTable("dbo.Zona");
            DropTable("dbo.Proveedor");
            DropTable("dbo.Presentacion");
            DropTable("dbo.Marca");
            DropTable("dbo.Producto");
        }
    }
}

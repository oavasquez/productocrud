﻿namespace ProductoCRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Producto", name: "id_marca_id_marca", newName: "id_marca");
            RenameColumn(table: "dbo.Producto", name: "id_presentacion_id_presentacion", newName: "id_presentacion");
            RenameColumn(table: "dbo.Producto", name: "id_provvedor_id_proveedor", newName: "Proveedor_id_proveedor");
            RenameColumn(table: "dbo.Producto", name: "id_zona_id_zona", newName: "id_zona");
            RenameIndex(table: "dbo.Producto", name: "IX_id_marca_id_marca", newName: "IX_id_marca");
            RenameIndex(table: "dbo.Producto", name: "IX_id_presentacion_id_presentacion", newName: "IX_id_presentacion");
            RenameIndex(table: "dbo.Producto", name: "IX_id_zona_id_zona", newName: "IX_id_zona");
            RenameIndex(table: "dbo.Producto", name: "IX_id_provvedor_id_proveedor", newName: "IX_Proveedor_id_proveedor");
            AddColumn("dbo.Producto", "id_provvedor", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Producto", "id_provvedor");
            RenameIndex(table: "dbo.Producto", name: "IX_Proveedor_id_proveedor", newName: "IX_id_provvedor_id_proveedor");
            RenameIndex(table: "dbo.Producto", name: "IX_id_zona", newName: "IX_id_zona_id_zona");
            RenameIndex(table: "dbo.Producto", name: "IX_id_presentacion", newName: "IX_id_presentacion_id_presentacion");
            RenameIndex(table: "dbo.Producto", name: "IX_id_marca", newName: "IX_id_marca_id_marca");
            RenameColumn(table: "dbo.Producto", name: "id_zona", newName: "id_zona_id_zona");
            RenameColumn(table: "dbo.Producto", name: "Proveedor_id_proveedor", newName: "id_provvedor_id_proveedor");
            RenameColumn(table: "dbo.Producto", name: "id_presentacion", newName: "id_presentacion_id_presentacion");
            RenameColumn(table: "dbo.Producto", name: "id_marca", newName: "id_marca_id_marca");
        }
    }
}

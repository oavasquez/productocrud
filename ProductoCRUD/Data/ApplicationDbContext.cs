﻿using Microsoft.AspNet.Identity.EntityFramework;
using ProductoCRUD.EntityModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductoCRUD.Data
{
   



        public class ApplicationDbContext : DbContext
        {

            public ApplicationDbContext()
                : base("ConnectionStringSQL")
            {


            }

        public DbSet<Marca> Marca { get; set; }
        public DbSet<Presentacion> Presentacion { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<Proveedor> Proveedor { get; set; }
        public DbSet<Zona> Zona { get; set; }




    }
    
}